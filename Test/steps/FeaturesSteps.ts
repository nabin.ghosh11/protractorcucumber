import { defineSupportCode } from 'cucumber'
import { HomePage } from "../pages/HomePage";
import { expect } from 'chai'
import {FeaturesPage} from "../pages/FeaturesPage";


defineSupportCode(({Given, When, Then, }) => {
    
    var featuresPage = new FeaturesPage();

   

    When(/^I verify features page$/, async()=> {
        console.log("in step def ");
        await featuresPage.getHeadingName().then((text1) => {
            console.log("text-----"+text1);
            expect(text1).to.be.equal("FEATURES & BENEFITS");
     })
    });
});