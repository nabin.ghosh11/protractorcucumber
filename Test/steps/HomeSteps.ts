import { defineSupportCode } from 'cucumber'
import { HomePage } from "../pages/HomePage";
import { expect } from 'chai'
import { CourseDetailsPage } from "../pages/CourseDetails";
import { browser } from 'protractor';

defineSupportCode(({Given, When, Then, }) => {
    
    var homePage = new HomePage();
    var coursedetails = new CourseDetailsPage();

    Given(/^I navigate to application$/, async () => {
        await homePage.OpenBrowser("https://angular.io/");
    });

     When(/^I click on features$/, async()=> {
       await homePage.clickFeatures();
    })

    When(/^I click on docs$/, async()=> {
    })

    // When(/^I get all the heading$/, async () => {
    //     await homePage.GetAllHeadings();
    // });

    // When(/^I click the '([^\"]*)' course$/, async (headingText) => {
    //     await homePage.ClickFirstHeading(headingText.toString());
    // });

    // Then(/^I should see '([^\"]*)' course in coursedetails page$/, async (course) => {
    //     expect(coursedetails.GetCourseHeading).to.be.null;
    // });

});