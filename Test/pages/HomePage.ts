//import the class
import { browser, element, by, protractor, $$, $ } from 'protractor';
import { IdentificationType, BasePage } from "./BasePage";


const Locators = {
    heading: {
        type:IdentificationType[IdentificationType.Xpath],
        value: "//course-thumb/div/h2[text()=' Selenium Framework development ']"
    },

    getStartedButton: {
        type: IdentificationType[IdentificationType.Css],
        value: "a.button"
    },

    featuresLink :{
        type: IdentificationType[IdentificationType.Css],
        value: "a[title='Features']"
    },

    docLink :{
        type: IdentificationType[IdentificationType.Css],
        value: "a[title='Docs']"
    }
}


export class HomePage extends BasePage {

    //Selenium framework development course heading
   // heading = this.ElementLocator(Locators.heading).element(by.xpath("//span[contains(text(),'4th')]"));
    getStartedButton =this.ElementLocator(Locators.getStartedButton);
    featuresLink=this.ElementLocator(Locators.featuresLink);
    docLink=this.ElementLocator(Locators.docLink)


    //All heading                           
   // headings = this.ElementLocator(Locators.headings);

    //Open browser
    async OpenBrowser(url: string){
        await browser.get(url);
    }

    async clickFeatures(){
        await this.featuresLink.click();
    }

   async clickDoc(){
       await this.docLink.click();
   }

}

